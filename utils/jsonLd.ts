import { seoConfig } from "./seoConfig";
import { getImage } from "astro:assets";
import logo from "@img/okestream.png"
import featured from "@img/okestream-featured.jpg"
const optimizedLogo = await getImage({ src: logo, format: 'avif' })
const optimizedFeatured = await getImage({ src: featured, format: 'avif' })

export const jsonLd = {
	org: {
		"@context": "https://schema.org",
		"@type": "Organization",
		"name": seoConfig.siteName,
		"alternateName": seoConfig.siteName,
		"url": seoConfig.baseURL,
		"logo": optimizedLogo.src,
		"sameAs": seoConfig.baseURL
	},
	article: {
		"@context": "https://schema.org",
		"@type": "Article",
		"mainEntityOfPage": {
			"@type": "WebPage",
			"@id": seoConfig.baseURL
		},
		"headline": seoConfig.title,
		"description": seoConfig.description,
		"image": [
			optimizedFeatured.src
		],
		"author": {
			"@type": "Organization",
			"name": seoConfig.siteName,
			"url": seoConfig.baseURL
		},
		"publisher": {
			"@type": "Organization",
			"name": seoConfig.siteName,
			"logo": {
				"@type": "ImageObject",
				// "url": seoConfig.publisher.logo
				"url": optimizedLogo.src
			}
		}
	},

	breadcrumbList: {
		"@context": "https://schema.org/",
		"@type": "BreadcrumbList",
		"itemListElement": [
			{
				"@type": "ListItem",
				"position": 1,
				"name": "Home",
				"item": seoConfig.baseURL
			},
			{
				"@type": "ListItem",
				"position": 2,
				"name": seoConfig.author.name,
				"item": seoConfig.baseURL
			},
			{
				"@type": "ListItem",
				"position": 3,
				"name": seoConfig.title
			}
		]
	}
}
