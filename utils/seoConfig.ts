// Type imports
import type { ManifestOptions } from "vite-plugin-pwa"


/**
 * Defines the default SEO configuration for the website.
 */
const baseUrl = "https://okestream1.fun"
const featuredImage = {
	url: 'https://1.bp.blogspot.com/-KhXcti77KfI/Xup0ikMNTsI/AAAAAAAALIc/YOKkFWhjntg9QAUf4-yjTxZF1jPZftHQQCLcBGAsYHQ/s1600/Okestream%2BFeature.jpg',
	alt: "OKESTREAM"
}
const logo = {
	url: '/OKESTREAM-logo.png',
	alt: "OKESTREAM Logo"
}
const publisher = {
	name: "OKESTREAM",
	url: baseUrl,
	logo: "https://1.bp.blogspot.com/-KhXcti77KfI/Xup0ikMNTsI/AAAAAAAALIc/YOKkFWhjntg9QAUf4-yjTxZF1jPZftHQQCLcBGAsYHQ/s1600/Okestream%2BFeature.jpg",
}
const author = {
	name: "OKESTREAM",
	url: baseUrl,
	logo: "https://1.bp.blogspot.com/-KhXcti77KfI/Xup0ikMNTsI/AAAAAAAALIc/YOKkFWhjntg9QAUf4-yjTxZF1jPZftHQQCLcBGAsYHQ/s1600/Okestream%2BFeature.jpg",
}
const siteName = "OKESTREAM",
	siteLink = 'https://s.id/okestream',
	siteDirectLink = 'https://www.highcpmrevenuegate.com/f0y7m57828?key=0a213b0d1b3a035821ba340ea1749227',
	siteShortName = "OKESTREAM",
	siteTitle = "OKESTREAM - Streaming Sepakbola dan Basket Terbaik",
	siteDescription = "OKESTREAM - Platform terbaik untuk menonton streaming sepakbola dan basket secara online.",
	keywords = "OKESTREAM streaming, OKESTREAM sepakbola, OKESTREAM basket, OKESTREAM terbaik, live streaming sepakbola OKESTREAM, live streaming basket OKESTREAM, OKESTREAM online, OKESTREAM gratis, OKESTREAM HD, OKESTREAM nonton sepakbola, OKESTREAM nonton basket, OKESTREAM streaming terbaik, OKESTREAM streaming sepakbola terbaik, OKESTREAM streaming basket terbaik, OKESTREAM legal, OKESTREAM resmi, OKESTREAM website, OKESTREAM platform, OKESTREAM sports, OKESTREAM live sports, OKESTREAM streaming sports, OKESTREAM soccer streaming, OKESTREAM basketball streaming, OKESTREAM watch live sports, OKESTREAM sports streaming."



export const seoConfig = {
	publisher, author, siteLink, keywords, siteDirectLink,
	title: siteTitle,
	baseURL: baseUrl, // Change this to your production URL.
	description: siteDescription, // Change this to be your website's description.
	type: "website",
	image: {
		url: featuredImage.url, // Change this to your website's thumbnail.
		alt: featuredImage.alt, // Change this to your website's thumbnail description.
		width: 1200,
		height: 630
	},
	logo,
	siteName: siteName, // Change this to your website's name,
	twitter: {
		card: "summary_large_image"
	},
	extend: {
		// <link rel="preload" as="image" href="https://i.postimg.cc/yxtjcCFk/slot-gacor.jpg"/>
		link: [
			// {
			// 	rel: "preload", as: "image", href: "/OKESTREAM-logo.png",
			// },
		],
		meta: [
			{ name: "HandheldFriendly", content: "true" },
			{ name: "MobileOptimized", content: "width" },
			{ name: "language", content: "indonesia" },
			{ name: "page-locale", content: "id,en" },
		]
	}
}




/**
 * Defines the configuration for PWA webmanifest.
 */
export const manifest: Partial<ManifestOptions> = {
	name: siteName, // Change this to your website's name.
	short_name: siteShortName, // Change this to your website's short name.
	description: seoConfig.description, // Change this to your websites description.
	theme_color: "#30E130", // Change this to your primary color.
	background_color: "#ffffff", // Change this to your background color.
	display: "minimal-ui",
	icons: [
		{
			src: "/favicons/favicon-192x192.png",
			sizes: "192x192",
			type: "image/png"
		},
		{
			src: "/favicons/favicon-512x512.png",
			sizes: "512x512",
			type: "image/png"
		},
		{
			src: "/favicons/favicon-512x512.png",
			sizes: "512x512",
			type: "image/png",
			purpose: "any maskable"
		}
	]
}
